package com.alox1d.EGTest

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.alox1d.EGTest.EGTestActivityFragment.RESULT

/**
 * Created by Alox1d on 16.04.2018.
 */

class DialogHelper(private val context: Context) {

    private val factory: LayoutInflater? = null
    private lateinit var builder: AlertDialog.Builder
    private val view: View? = null
    private val dialog: Dialog? = null

    fun initializeAboutDialog() {
        builder = AlertDialog.Builder(context)
        val factory = LayoutInflater.from(context)
        val view = factory.inflate(R.layout.dialog_about_program, null)
        builder.setView(view)
        builder.setNegativeButton(R.string.close) { dlg, sumthin -> }
        builder.show()
    }


}

class ResultFragment() : DialogFragment() {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!)

        val string = arguments!!.getString(RESULT)

        builder.setMessage(string)

        // "Reset Quiz" Button
        builder.setPositiveButton(R.string.reset_quiz
        ) { dialog, id ->
            val listener = targetFragment as DialogListener
            listener.onFinishEditDialog()
        }

        return builder.create() // return the AlertDialog
        //                                    return super.onCreateDialog(savedInstanceState);
    }

    interface DialogListener {
        fun onFinishEditDialog()
    }

}
