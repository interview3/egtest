package com.alox1d.EGTest

import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.alox1d.EGTest.adapters.MainNavAdapter
import com.alox1d.EGTest.models.TestThemeItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        val adapter = MainNavAdapter()
        mainNavItems.adapter = adapter

        val keys: Array<String> = getResources().getStringArray(R.array.tests_theme)
        val data = mutableListOf<TestThemeItem>()
        keys.forEachIndexed { index, s ->
            data.add(TestThemeItem(s, index + 1))
        }

        adapter.data = data
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // get the device's current orientation
        val orientation = resources.configuration.orientation

        // display the app's menu only in portrait orientation
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            // inflate the menu
            menuInflater.inflate(R.menu.menu_main, menu)
            return true
        } else
            return false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //      Intent preferencesIntent = new Intent(this, SettingsActivity.class);
        //      startActivity(preferencesIntent);
        val dialogHelper = DialogHelper(this)
        dialogHelper.initializeAboutDialog()
        return super.onOptionsItemSelected(item)
    }
}
