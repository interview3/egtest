package com.alox1d.EGTest.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.alox1d.EGTest.EGTestActivity
import com.alox1d.EGTest.R
import com.alox1d.EGTest.models.TEST_THEME_ITEM
import com.alox1d.EGTest.models.TestThemeItem

class MainNavAdapter() : RecyclerView.Adapter<MainNavAdapter.ViewHolder>() {


    var data = listOf<TestThemeItem>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_item_test, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.testName.text = item.name
        holder.testNum.text = item.testNum.toString()

        holder.card.setOnClickListener { view ->
            val intent = Intent(view.context, EGTestActivity::class.java)
            intent.putExtra(TEST_THEME_ITEM, item)
            view.context.startActivity(intent)
        }
    }


    override fun getItemCount(): Int = data.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val testName: TextView = itemView.findViewById(R.id.testName_tv)
        val testNum: TextView = itemView.findViewById(R.id.testNumber_tv)
        val card: CardView = itemView.findViewById(R.id.cardView)
    }
}


