package com.alox1d.EGTest.models

import java.io.Serializable

const val TEST_THEME_ITEM = "TEST_THEME_ITEM"

data class TestThemeItem(
        var name: String,
        var testNum: Int
) : Serializable
